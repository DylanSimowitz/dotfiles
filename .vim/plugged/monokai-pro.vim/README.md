# monokai-pro.vim

The theme includes the following variants:
- Monokai Pro
- Monokai Pro (Filter Octagon)
- Monokai Pro (Filter Machine)
- Monokai Pro (Filter Ristretto)
- Monokai Pro (Filter Spectre)
- Monokai Classic

A port of the Monokai Pro Theme by Wimer Hazenberg for Vim and NeoVim.  
I wrote this for personal use, so, if you don't like some highlight groups, you can still add them to your .vimrc / init.vim to change what you want.
