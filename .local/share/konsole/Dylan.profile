[Appearance]
ColorScheme=Monokai Pro
Font=Operator Mono  SSm Lig,11,-1,5,50,0,0,0,0,0

[General]
DimWhenInactive=false
Name=Dylan
Parent=FALLBACK/
TerminalCenter=true
TerminalMargin=8

[Scrolling]
ScrollBarPosition=2
