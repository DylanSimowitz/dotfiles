# dotfiles
## Installation

This repository uses homemaker to manage symlinks and install dependencies. Simply run `homemaker config.toml .` from within
the cloned repository on an arch linux distribution to install.

## Preview

![preview](https://i.imgur.com/Uc6jzP8.png)
---
![preview2](https://i.imgur.com/jKVgTjx.jpg)
