
# macros
#

[macros.clone]
    deps = ["git"]
    prefix = ["git", "clone"]

[macros.install]
    deps = ["yay"]
    prefix = ["yay", "-S", "--noconfirm", "--noeditmenu", "--needed"]

[macros.pacman]
    prefix = ["sudo", "pacman", "-Syu", "--noconfirm"]

[macros.go-get]
    deps = ["golang"]
    prefix = ["go", "get"]

[macros.npm-install]
    deps = ["node"]
    prefix = ["sudo", "npm", "install", "-g"]

[macros.pip-install]
    prefix = ["sudo", "pip", "install"]

[macros.gem-install]
    prefix = ["gem", "install"]


#
# development
#

[tasks.dev]
    deps = ["vim", "emacs"]
    cmds = [
        ["@install", "the_silver_searcher"],
    ]

[tasks.git]
    rejects = [["which", "git"]]
    cmds = [["@install", "git"]]
    links = [[".gitconfig"]]

[tasks.yay]
    rejects = [["which", "yay"]]
    cmds = [
	["mkdir", "-p", "/tmp/yay_install"],
	["cd", "/tmp/yay_install"],
        ["@clone", "https://aur.archlinux.org/yay.git"],
        ["cd", "yay"],
        ["makepkg", "-si"],
	["rm", "-r", "/tmp/yay_install"],
    ]

[tasks.node]
    cmds = [["@install", "nodejs"]]

#
# editors
#

[tasks.emacs]
    cmds = [
        ["@install", "emacs"],
        ["@clone", "https://github.com/syl20bnr/spacemacs", "$HM_DEST/.emacs.d"],
        ["@npm-install", "tern"],
    ]
    links = [[".spacemacs"]]

[tasks.vim]
    cmds = [["@install", "vim", "neovim"]]
    links = [[".vimrc"], [".vim"], [".config/nvim"]]

[tasks.vscode]
    deps = ["fonts"]
    cmds = [
        ["@install", "code-transparent"],
    ]
    links = [[".config/Code - OSS/User/settings.json"]]

[tasks.editors]
    deps=["emacs", "vim", "vscode"]

#
# desktop
#

[tasks.i3]
    cmds = [
        ["@install", "i3-gaps-git", "compton"],
        ["@gem-install", "i3ipc"]
    ]
    links = [
        [".config/i3/config"],
    ]

[tasks.polybar]
    cmds = [
        ["@install", "polybar", "ttf-material-design-icons-git", "ttf-font-awesome"],
    ]
    links = [
        [".config/polybar/config"],
        [".config/polybar/launch.sh"],
    ]

[tasks.wal]
    cmds = [
        ["@install", "python-pywal", "feh"],
        ["mkdir", "-p", "$HM_DEST/Pictures/Wallpapers"]
    ]
    links = [
        [".config/wal"],
    ]

[tasks.gtk]
    cmds = [
        ["@install", "gtk-arc-flatabulous-theme-git", "numix-icon-theme-git"],
    ]
    links = [
        [".config/gtk-3.0/settings.ini"],
        [".config/gtk-3.0/gtk.css"],
    ]

[tasks.xorg]
    cmds = [
        ["@install", "xorg-server", "xorg-apps", "lightdm"],
    ]
    links = [
        [".Xresources"],
        [".xinitrc"],
        [".xprofile"],
    ]

[tasks.dunst]
    cmds = [
        ["@install", "dunst"],
    ]
    links = [
        [".config/dunst/dunstrc"],
    ]


[tasks.rofi]
    cmds = [
        ["@install", "rofi"],
    ]
    links = [
        [".config/rofi/config"],
    ]

[tasks.kde]
    deps=["fonts", "icons"]
    links = [
        [".config/Kvantum"],
        [".config/latte"],
        [".config/lattedockrc"],
        [".local/share/konsole"],
    ]

[tasks.desktop]
    deps=["i3", "polybar", "wal", "powerline", "xorg", "gtk", "dunst", "rofi", "kde"]


#
# general
#

[tasks.zsh]
    deps=["powerline"]
    cmds = [["@install", "zsh"], ["chsh", "-s", "/usr/bin/zsh"]]
    links = [
        [".config/zsh/.zshrc"],
        [".config/zsh/.zshenv"],
        [".zshenv"]
    ]

[tasks.powerline]
    cmds = [
        ["@install", "powerline"],
    ]
    links = [
        [".config/powerline"],
    ]

[tasks.common-term]
    cmds = [["@install", "openssh", "htop", "traceroute", "tree", "tmux-git", "whois", "rsync", "thefuck"]]
    links = [["bin"], [".tmux.conf"]]

[tasks.term]
    deps = ["common-term", "zsh"]
    cmds = [
        ["@install", "termite", "ttf-iosevka-term"],
    ]
    links = [[".config/termite/options"]]

[tasks.fonts]
    links = [[".local/share/fonts"]]

[tasks.icons]
    links = [[".local/share/icons/macOS-icons"]]



[tasks.default]
    deps = ["term", "dev", "kde"]
